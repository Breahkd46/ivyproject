package com.company;

import com.company.fusion.Actioner;
import com.company.fusion.Position;
import com.company.fusion.SRAReco;
import com.company.fusion.States;
import fr.dgac.ivy.Ivy;
import fr.dgac.ivy.IvyException;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static com.company.fusion.States.*;


/**
 * Fusion allows to merge oral order and drawn order (on the palette)
 */
public class Fusion {

    /**
     * @param args
     * @throws IvyException
     */
    public static void main(String[] args) throws IvyException {

        System.out.println("START");

        Ivy bus = new Ivy("fusion", "Start Fusion", null);

        bus.start("127.255.255.255:2010");

        SRAReco sraReco = new SRAReco(bus);



        /* ##############################################
         *                       Init VAR
         * ###############################################
         * */

        AtomicReference<States> state = new AtomicReference<>(Att_Action);

        AtomicReference<String> opt_couleur = new AtomicReference<>("");
        AtomicReference<Position> opt_position = new AtomicReference<>(null);
        AtomicReference<String> objet = new AtomicReference<>("");
        AtomicReference<String> form = new AtomicReference<>("");
        Timer timer = new Timer("Timer");

        Actioner actioner = new Actioner(bus, timer);

        /* ##############################################
        *                       EVENTS
        * ###############################################
        * */


        /*
         * E_Timer
         * */

        class TimerTaskFusion extends TimerTask {

            @Override
            public void run() {
                System.out.println("State = " + state.get());
                switch (state.get()) {
                    case Att_Option_Creation:
                        actioner.creer_Object(opt_position.get(), opt_couleur.get(), form.get());
                        actioner.stop_Timer();
                        actioner.reset_Variable(objet, opt_position, opt_couleur, form);
                        state.set(Att_Action);
                        break;
                    case Att_Option_Couleur_Deplacer:
                        actioner.dep_Object(objet.get(), opt_position.get(), opt_couleur.get());
                        actioner.stop_Timer();
                        actioner.reset_Variable(objet, opt_position, opt_couleur, form);
                        state.set(Att_Action);
                        break;
                    case Att_Option_Supprimer:
                        actioner.sup_Object(objet.get());
                        actioner.stop_Timer();
                        actioner.reset_Variable(objet, opt_position, opt_couleur, form);
                        state.set(Att_Action);
                        break;
                }
            }
        }

        /*
        * E_Creer_Rectangle
        * */

        bus.bindMsg("Reco:Creer_Rectangle",(client, s) -> {
            System.out.println("State = " + state.get());
            if (state.get() == States.Att_Action) {
                System.out.println("Creer_Rectangle ");
                actioner.set_form(form, "Rectangle");
                state.set(Att_Option_Creation);
                actioner.reset_Timer(new TimerTaskFusion());
            }
        });

        /*
         * E_Creer_Ellipse
         * */

        bus.bindMsg("Reco:Creer_Ellipse",(client, s) -> {
            System.out.println("State = " + state.get());
            if (state.get() == States.Att_Action) {
                System.out.println("Creer_Ellipse");
                actioner.set_form(form, "Ellipse");
                state.set(Att_Option_Creation);
                actioner.reset_Timer(new TimerTaskFusion());
            }
        });

        /*
        * E_Pointage
        * */

        bus.bindMsg("Palette:MouseMoved x=(.*) y=(.*)",(client, s) -> {
            switch (state.get()) {
                case Att_Pointage_Creation:
                    if (!opt_couleur.get().equals("")) {
                        state.set(Att_Action);
                        actioner.set_Postion(opt_position, s[0], s[1]);
                        actioner.creer_Object(opt_position.get(), opt_couleur.get(), form.get());
                        actioner.reset_Variable(objet, opt_position, opt_couleur, form);
                    } else {
                        actioner.reset_Timer(new TimerTaskFusion());
                        actioner.set_Postion(opt_position, s[0], s[1]);
                        state.set(Att_Option_Creation);
                    }
                    break;
                case Att_Designation_Couleur:
                    if (opt_position.get() != null) {
                        actioner.set_Couleur_By_Position(opt_couleur, new Position(s[0], s[1]), "", () -> {
                            System.out.println("couleur : " + opt_couleur.get());
                            actioner.creer_Object(opt_position.get(), opt_couleur.get(), form.get());
                            actioner.reset_Variable(objet, opt_position, opt_couleur, form);
                            state.set(Att_Action);
                        });
                    } else {
                        actioner.reset_Timer(new TimerTaskFusion());
                        actioner.set_Couleur_By_Position(opt_couleur, new Position(s[0], s[1]), "");
                        state.set(Att_Option_Creation);
                    }
                    break;
                case Att_Voix_Designation:
                    actioner.set_Object(Integer.parseInt(s[0]), Integer.parseInt(s[1]), form.get(), objet);
                    state.set(Att_Position_Deplacer);
                    break;
                case Att_Pointage_Position:
                    actioner.set_Postion(opt_position, s[0], s[1]);
                    state.set(Att_Designation_Deplacer);
                    break;
                case Att_Pointage_Position_Post_Designation:
                    actioner.set_Postion(opt_position, s[0], s[1]);
                    actioner.dep_Object(objet.get(), new Position(s[0], s[1]), opt_couleur.get());
                    actioner.reset_Variable(objet, opt_position, opt_couleur, form);
                    state.set(Att_Action);
                    break;
                case Att_Voix_Designation_Post_Position:
                    actioner.set_Object(Integer.parseInt(s[0]), Integer.parseInt(s[1]), form.get(), objet);
                    state.set(Att_Option_Couleur_Deplacer);
                    break;
                case Att_Designation_Voix_Supprimer:
                    actioner.set_Postion(opt_position, s[0], s[1]);
                    actioner.set_Object(Integer.parseInt(s[0]), Integer.parseInt(s[1]), form.get(), objet);
                    actioner.reset_Timer(new TimerTaskFusion());
                    state.set(Att_Option_Supprimer);
                    break;
            }
        });

        /*
        * E_Voix_Position
        * */

        String[] words_Voix_Position = {"ici", "la", "a cette position"};

        sraReco.bindMessageSRAWord(words_Voix_Position, word -> {
            System.out.println("State = " + state.get());
            switch (state.get()) {
                case Att_Option_Creation:
                    state.set(Att_Pointage_Creation);
                    break;
                case Att_Designation_Position_Deplacer:
                    state.set(Att_Pointage_Position);
                    break;
                case Att_Position_Deplacer:
                    state.set(Att_Pointage_Position_Post_Designation);
                    break;
            }
        });

        /*
        * E_Voix_Couleur
        * */

        String[] words_Voix_Couleur = new String[]{"noir", "bleu", "rouge", "jaune", "vert"};

        sraReco.bindMessageSRAWord(words_Voix_Couleur, color -> {
            System.out.println("State = " + state.get());
            switch (state.get()) {
                case Att_Option_Creation:
                    actioner.set_Couleur(opt_couleur, color);
                    if (!opt_couleur.get().equals("")) {
                        actioner.creer_Object(opt_position.get(), opt_couleur.get(), form.get());
                        actioner.reset_Variable(objet, opt_position, opt_couleur, form);
                        state.set(Att_Action);
                    } else {
                        actioner.set_Couleur(opt_couleur, color);
                        actioner.reset_Timer(new TimerTaskFusion());
                    }
                    break;
                case Att_Position_Deplacer:
                    actioner.set_Couleur(opt_couleur, color);
                    break;
                case Att_Option_Couleur_Deplacer:
                    actioner.set_Couleur(opt_couleur, color);
                    actioner.dep_Object(objet.get(), opt_position.get(), opt_couleur.get());
                    actioner.reset_Variable(objet, opt_position, opt_couleur, form);
                    state.set(Att_Action);
                    break;
                case Att_Option_Supprimer:
                    actioner.set_Objet_By_Couleur_Position(opt_position.get(), color, form.get(), received -> {
                        actioner.sup_Object(received);
                        actioner.reset_Variable(objet, opt_position, opt_couleur, form);
                        state.set(Att_Action);
                    });
                    break;
            }
        });


        /*
        * E_Deplacer
        * */

        bus.bindMsg("Reco:Deplacer",(client, s) -> {
            System.out.println("State = " + state.get());
            if (state.get() == States.Att_Action) {
                System.out.println("Deplacer");
                state.set(Att_Designation_Position_Deplacer);
            }
        });


        /*
        * E_Voix_Designation
        * */

        String[] words_Voix_Designation = new String[]{"cet objet", "ce rectangle", "cette ellipse"};

        sraReco.bindMessageSRAWord(words_Voix_Designation, word -> {
            System.out.println("State = " + state.get());
            switch (state.get()) {
                case Att_Designation_Position_Deplacer:
                    state.set(Att_Voix_Designation);
                    if (word.equals("cet objet")) {
                        actioner.set_form(form, "");
                    }else if (word.equals("ce rectangle")) {
                        actioner.set_form(form, "Rectangle");
                    } else {
                        actioner.set_form(form, "Ellipse");
                    }
                    break;
                case Att_Designation_Deplacer:
                    state.set(Att_Voix_Designation_Post_Position);
                    if (word.equals("cet objet")) {
                        actioner.set_form(form, "");
                    }else if (word.equals("ce rectangle")) {
                        actioner.set_form(form, "Rectangle");
                    } else {
                        actioner.set_form(form, "Ellipse");
                    }
                    break;
                case Att_Pointage_Supprimer:
                    state.set(Att_Designation_Voix_Supprimer);
                    if (word.equals("cet objet")) {
                        actioner.set_form(form, "");
                    }else if (word.equals("ce rectangle")) {
                        actioner.set_form(form, "Rectangle");
                    } else {
                        actioner.set_form(form, "Ellipse");
                    }
                    break;
            }
        });


        /*
        * E_Supprimer
        * */

        bus.bindMsg("Reco:Supprimer",(client, s) -> {
            System.out.println("State = " + state.get());
            if (state.get() == States.Att_Action) {
                System.out.println("Supprimer");
                state.set(Att_Pointage_Supprimer);
            }
        });


        /*
        * E_Voix_Designation_Couleur = "de cette couleur"
        * */

        String[] words_Voix_Designation_Couleur = new String[]{"de cette couleur"};

        sraReco.bindMessageSRAWord(words_Voix_Designation_Couleur, word -> {
            System.out.println("State = " + state.get());
            switch (state.get()) {
                case Att_Option_Creation:
                    state.set(Att_Designation_Couleur);
                    break;
                case Att_Designation_Deplacer:
                    state.set(Att_Voix_Designation_Post_Position);
                    break;
                case Att_Pointage_Supprimer:
                    state.set(Att_Designation_Voix_Supprimer);
                    break;
            }
        });
    }
}
