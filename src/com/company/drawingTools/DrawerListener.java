package com.company.drawingTools;

import java.util.EventListener;

public interface DrawerListener<T> extends EventListener {

    void receive(T received);
}
