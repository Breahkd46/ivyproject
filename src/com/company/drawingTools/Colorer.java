package com.company.drawingTools;

public class Colorer {

    public static String convertColorFromName(String name) {
        switch (name) {
            case "jaune":
                return "255:255:0";
            case "rouge":
                return "255:0:0";
            case "bleu":
                return "0:0:255";
            case "vert":
                return "0:255:255";
            default:
                return "0:0:0"; // NOIR
        }
    }

    public static String translateColorFromFrench(String color) {
        switch (color) {
            case "jaune":
                return "yellow";
            case "rouge":
                return "red";
            case "bleu":
                return "cyan";
            case "vert":
                return "green";
            default:
                return "black"; // NOIR
        }
    }
}
