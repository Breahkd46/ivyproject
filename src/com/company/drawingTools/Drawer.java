package com.company.drawingTools;

import fr.dgac.ivy.Ivy;
import fr.dgac.ivy.IvyException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

public class Drawer {

    public static void startDraw(AtomicInteger compteurInit, Ivy bus, String x, String y) {
        Drawer.startDraw(compteurInit, bus, Integer.parseInt(x), Integer.parseInt(y));
    }


    public static void startDraw(AtomicInteger compteurInit, Ivy bus, int x, int y) {
        try {
            bus.sendMsg("Palette:CreerRectangle x=" + x + " y=" + y + " longueur=1 hauteur=1");
            bus.bindMsgOnce("Palette:ResultatTesterPoint x=" + x + " y=" + y + " nom=R(.*)", (client, s) -> {
                System.out.println("Pressed : " + s[0]);
                compteurInit.set(Integer.parseInt(s[0]));
            });
            bus.sendMsg("Palette:TesterPoint x=" + x + " y=" + y);
        } catch (IvyException e) {
            e.printStackTrace();
        }
    }

    public static void continuDraw(Ivy bus, String x, String y) {
        Drawer.continuDraw(bus, Integer.parseInt(x), Integer.parseInt(y));
    }

    public static void continuDraw(Ivy bus, int x, int y) {
        try {
            bus.sendMsg("Palette:CreerRectangle x=" + x + " y=" + y + " longueur=1 hauteur=1");
        } catch (IvyException e) {
            e.printStackTrace();
        }
    }

    public static void stopDraw(AtomicInteger compteurFinal, Ivy bus, String x, String y) {
        Drawer.stopDraw(compteurFinal, bus, Integer.parseInt(x), Integer.parseInt(y));
    }

    public static void stopDraw(AtomicInteger compteurFinal, Ivy bus, int x, int y) {
        try {
            bus.bindMsgOnce("Palette:ResultatTesterPoint x=" + x + " y=" + y + " nom=R(.*)", (client2, s) -> {
                System.out.println("Released : " + s[0]);
                compteurFinal.set(Integer.parseInt(s[0]));
            });
            bus.sendMsg("Palette:TesterPoint x=" + x + " y=" + y);
        } catch (IvyException e) {
            e.printStackTrace();
        }
    }

    public static void deleteDraw(int compteurInit, int compteurFinal, Ivy bus) {
        for(int i = compteurInit ;i <= compteurFinal; i++) {
            try {
                bus.sendMsg("Palette:SupprimerObjet nom=R" + i);
            } catch (IvyException e) {
                e.printStackTrace();
            }
        }
    }

    public static void deleteDraw(AtomicInteger compteurInit, AtomicInteger compteurFinal, Ivy bus) {
        Drawer.deleteDraw(compteurInit.get(), compteurFinal.get(), bus);
    }

    public static void getObjectByPoints(Ivy bus, int x, int y, String form, DrawerListener<List<String>> drawerListener) {
        try {
            List<String> names = new ArrayList<>();

            String forme = (form.length() > 0 ? form.substring(0,1) : "");

            bus.bindMsgOnce("Palette:FinTesterPoint x=" + x + " y=" + y, (client, s) ->
                    drawerListener.receive(names));
            bus.bindMsgOnce("Palette:ResultatTesterPoint x=" + x + " y=" + y + " nom=" + forme + "(.*)", (client, s) -> {
                System.out.println("Released : " + forme + s[0]);
                names.add(forme + s[0]);
            });
            bus.sendMsg("Palette:TesterPoint x=" + x + " y=" + y);
        } catch (IvyException e) {
            e.printStackTrace();
        }
    }

    public static void getColorInfoObject(Ivy bus, String name, DrawerListener<String> drawerListener) {
        try {
            bus.bindMsgOnce("Palette:Info nom=" + name + " x=(.*) y=(.*) longueur=(.*) hauteur=(.*) couleurFond=(.*) couleurContour=(.*)", (client, s) ->
                    drawerListener.receive(s[4]));
            bus.sendMsg("Palette:DemanderInfo nom=" + name);
        } catch (IvyException e) {
            e.printStackTrace();
        }
    }
}
