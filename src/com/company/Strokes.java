package com.company;

import java.io.*;
import java.util.HashMap;

public class Strokes implements Serializable {

    private HashMap<String, Stroke> strokes;

    private static final String filepath= System.getProperty("user.dir") + "/templates.txt";

    public Strokes(HashMap<String, Stroke> strokes) {
        this.strokes = strokes;
    }

    public Strokes() {
        this.strokes = new HashMap<>();
    }

    public void addStroke(Stroke stroke, String name) {
        System.out.println("Strocke added : " + name);
        this.strokes.put(name, new Stroke(stroke));
    }

    public HashMap<String, Stroke> getStrokes() {
        return strokes;
    }

    public void save() {
        try {
            FileOutputStream fileOut = new FileOutputStream(filepath);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(this.strokes);
            objectOut.close();
            System.out.println("Saved !");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void load() {

        try {
            File file = new File(filepath);
            if (!file.createNewFile()) {
                FileInputStream fileIn = new FileInputStream(file);

                ObjectInputStream objectIn = new ObjectInputStream(fileIn);

                this.strokes = (HashMap<String, Stroke>) objectIn.readObject();

                System.out.println("The Object has been read from the file");
                objectIn.close();
            }
        } catch (EOFException e) {
            System.out.println("The file : " + filepath + " is empty");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println("Nb strockes = " + this.strokes.size());
        this.strokes.forEach((stroke, s) -> System.out.println("    " + s));
    }
}
