package com.company.fusion;

import com.company.drawingTools.Colorer;
import com.company.drawingTools.Drawer;
import com.company.fusion.listener.ActionListener;
import com.company.fusion.listener.ActionReceivedListener;
import fr.dgac.ivy.Ivy;
import fr.dgac.ivy.IvyException;

import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicReference;

public class Actioner {

    private static final int THRESHOLD = 7000;

    private final Ivy bus;

    private final Timer timer;

    private TimerTask task;

    public Actioner(Ivy bus, Timer timer) {
        this.bus = bus;
        this.timer = timer;
        this.task = null;
    }

    public void creer_Object(Position position, String couleur, String form) {
        System.out.println("Action : CREER_Object : " + form);

        position = (position == null) ? new Position(10,10) : position;

        try {
            this.bus.sendMsg("Palette:Creer" + form + " x=" + position.getX() + " y=" + position.getY() + (couleur.equals("") ? "" : " couleurFond=" + couleur));
        } catch (IvyException e) {
            e.printStackTrace();
        }
    }

    public void set_Postion(AtomicReference<Position> positionAtomicReference, String x, String y) {
        System.out.println("Action : SET_Postion");
        positionAtomicReference.set(new Position(x, y));
    }

    public void set_Couleur(AtomicReference<String> couleur, String couleurReplace) {
        System.out.println("Action : SET_Couleur");
        couleur.set(Colorer.convertColorFromName(couleurReplace));
    }

    public void sup_Object(String objet) {
        System.out.println("Action : SUP_Object");
        try {
            bus.sendMsg("Palette:SupprimerObjet nom=" + objet);
        } catch (IvyException e) {
            e.printStackTrace();
        }
    }

    public void dep_Object(String objet, Position position, String couleur) {
        System.out.println("Action : DEP_Object : " + objet);
        try {
            bus.sendMsg("Palette:DeplacerObjetAbsolu nom=" + objet + " x=" + position.getX() + " y=" + position.getY());
        } catch (IvyException e) {
            e.printStackTrace();
        }
    }

    public void set_Object(int x, int y, String form, AtomicReference<String> objet) {
        System.out.println("Action : SET_Object");
        Drawer.getObjectByPoints(bus, x, y, form, received -> received.stream().findFirst().ifPresent(objet::set));
    }

    public void reset_Timer(TimerTask task) {
        System.out.println("Action : RESET_Timer");
        if (this.task != null) {
            this.task.cancel();
        }
        this.task = task;
        this.timer.schedule(this.task, THRESHOLD);
    }

    public void stop_Timer() {
        System.out.println("Action : STOP_Timer");
        this.task.cancel();
    }

    public void set_Couleur_By_Position(AtomicReference<String> couleur, Position position, String form) {
        Drawer.getObjectByPoints(bus, position.getX(), position.getY(), form, figure -> {
            Optional<String> s = figure.stream().findFirst();
            s.ifPresent(value -> Drawer.getColorInfoObject(bus, value, couleur::set));
        });
    }

    public void set_Couleur_By_Position(AtomicReference<String> couleur,
                                        Position position, String form,
                                        ActionListener actionListener) {
        Drawer.getObjectByPoints(bus, position.getX(), position.getY(), form, figures -> {
            Optional<String> s = figures.stream().findFirst();
            s.ifPresent(value -> Drawer.getColorInfoObject(bus, value, color -> {
                couleur.set(color);
                actionListener.receive();
            }));
        });
    }

    public void reset_Variable(AtomicReference<String> objet,
                               AtomicReference<Position> position,
                               AtomicReference<String> couleur,
                               AtomicReference<String> form) {
        objet.set("");
        position.set(null);
        couleur.set("");
        form.set("");
    }

    public void set_Objet_By_Couleur_Position(Position position,
                                              String couleur,
                                              String form,
                                              ActionReceivedListener<String> actionReceivedListener) {
        Drawer.getObjectByPoints(bus, position.getX(), position.getY(),form, figures ->
                figures.forEach(s -> Drawer.getColorInfoObject(bus, s, color -> {
                    System.out.println("Color " + color);
                    if (Colorer.translateColorFromFrench(couleur).equals(color)) {
                        actionReceivedListener.receive(s);
                    }
        })));
    }

    public void set_form(AtomicReference<String> form, String formTo) {
        form.set(formTo);
    }
}
