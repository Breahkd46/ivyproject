package com.company.fusion;

import com.company.fusion.listener.SRARecoListener;
import fr.dgac.ivy.Ivy;
import fr.dgac.ivy.IvyException;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;

public class SRAReco {


    private Ivy bus;
    private int counter;

    private final Map<Integer, List<Integer>> bindedMessageSRAWords;

    private static final double THRESHOLD = 0.7;

    public SRAReco(Ivy bus) {
        this.bus = bus;
        this.bindedMessageSRAWords = new HashMap<>();
        this.counter = 0;
    }

    public int bindMessageSRAWord(String[] words, SRARecoListener sraRecoListener) {
        List<Integer> values = new ArrayList<>();
        int key = counter++;

        for (String word: words) {
            try {
                values.add(bus.bindMsg("sra5 Text=" + word + " Confidence=(.*)", (client, args) -> {
                    try {
                        if (NumberFormat.getNumberInstance(Locale.FRANCE).parse(args[0]).doubleValue() > 0.7) {
                            System.out.println("Recognisied word : " + word);
                            sraRecoListener.receive(word);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }));
            } catch (IvyException e) {
                e.printStackTrace();
            }
        }

        this.bindedMessageSRAWords.put(key, values);
        return key;
    }

    public void unBindMessageSRAWord(int key) {
        this.bindedMessageSRAWords.get(key).forEach(integer -> {
            try {
                this.bus.unBindMsg(integer);
            } catch (IvyException e) {
                e.printStackTrace();
            }
        });
    }
}
