package com.company.fusion;

public enum States {
    Att_Action,
    Att_Option_Creation,
    Att_Pointage_Creation,
    Att_Designation_Couleur,
    Att_Designation_Position_Deplacer,
    Att_Voix_Designation,
    Att_Pointage_Position,
    Att_Position_Deplacer,
    Att_Designation_Deplacer,
    Att_Pointage_Position_Post_Designation,
    Att_Voix_Designation_Post_Position,
    Att_Option_Couleur_Deplacer,
    Att_Pointage_Supprimer,
    Att_Designation_Voix_Supprimer,
    Att_Option_Supprimer
}
