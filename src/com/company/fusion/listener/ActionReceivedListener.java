package com.company.fusion.listener;

import java.util.EventListener;

public interface ActionReceivedListener<T> extends EventListener {
    void receive(T received);
}
