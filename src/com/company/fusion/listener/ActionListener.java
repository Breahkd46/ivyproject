package com.company.fusion.listener;

import java.util.EventListener;

public interface ActionListener extends EventListener {
    void receive();
}
