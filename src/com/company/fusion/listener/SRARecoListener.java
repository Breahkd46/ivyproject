package com.company.fusion.listener;

import java.util.EventListener;

public interface SRARecoListener extends EventListener {

    void receive(String word);
}
