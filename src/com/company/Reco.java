package com.company;

import com.company.drawingTools.Drawer;
import fr.dgac.ivy.Ivy;
import fr.dgac.ivy.IvyException;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Reco recognizes figures learned by Apprentissage and
 * draws a corresponding figure.
 *      Reco is able to just recognize:
 *      - suprimmer
 *      - cercle
 *      - rectangle
 */
public class Reco {

    public static void main(String[] args) throws IvyException {
        // write your code here

        Strokes strokes = new Strokes();
        strokes.load();

        AtomicInteger compteurInit = new AtomicInteger(0), compteurF = new AtomicInteger(0);

        System.out.println("START");

        Ivy bus = new Ivy("reco", "Start Reco", null);

        bus.start("127.255.255.255:2010");

        Stroke currentStroke = new Stroke();

        /*
        *
        * */

        bus.bindMsg("Palette:MousePressed x=(.*) y=(.*)", (client, s) -> {
            currentStroke.init();
            Drawer.deleteDraw(compteurInit, compteurF, bus);
            Drawer.startDraw(compteurInit, bus, s[0], s[1]);

            currentStroke.addPoint(Integer.parseInt(s[0]), Integer.parseInt(s[1]));
        });


        bus.bindMsg("Palette:MouseDragged x=(.*) y=(.*)", (client, s) -> {
            Drawer.continuDraw(bus, s[0], s[1]);
            currentStroke.addPoint(Integer.parseInt(s[0]), Integer.parseInt(s[1]));
        });



        double seuil = 50.0;

        bus.bindMsg("Palette:MouseReleased x=(.*) y=(.*)", (client, s) -> {
            Drawer.stopDraw(compteurF, bus, s[0], s[1]);

            if (currentStroke.listePoint.size() > 32) {
                currentStroke.normalize();

                AtomicReference<String> bestStroke = new AtomicReference<>("");
                AtomicReference<Double> bestDistance = new AtomicReference<>((double) 1000);

                strokes.getStrokes().forEach((name, stroke) -> {
                    double distance = currentStroke.calculDistance(stroke);
                    if (distance < bestDistance.get()) {
                        bestDistance.set(distance);
                        bestStroke.set(name);
                    }
                });

                if (bestDistance.get() < seuil) {
                    switch (bestStroke.get()) {
                        case "Creer_Rectangle":
                            try {
                                bus.sendMsg("Reco:Creer_Rectangle");
                            } catch (IvyException e) {
                                e.printStackTrace();
                            }
                            break;
                        case "Creer_Ellipse":
                            try {
                                bus.sendMsg("Reco:Creer_Ellipse");
                            } catch (IvyException e) {
                                e.printStackTrace();
                            }
                            break;
                        case "Deplacer":
                            try {
                                bus.sendMsg("Reco:Deplacer");
                            } catch (IvyException e) {
                                e.printStackTrace();
                            }
                            break;
                        case "Supprimer":
                            try {
                                bus.sendMsg("Reco:Supprimer");
                            } catch (IvyException e) {
                                e.printStackTrace();
                            }
                            break;
                    }
                }
            }


        });
    }
}
