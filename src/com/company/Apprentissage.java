package com.company;

import com.company.drawingTools.Drawer;
import fr.dgac.ivy.Ivy;
import fr.dgac.ivy.IvyException;

import javax.swing.*;
import java.util.concurrent.atomic.AtomicInteger;

import static javax.swing.JOptionPane.INFORMATION_MESSAGE;

/**
 * Apprentissage allows to learn figures drawn on the palette
 */
public class Apprentissage {

    public static void main(String[] args) throws IvyException {
	// write your code here

        Strokes strokes = new Strokes();
        strokes.load();

        AtomicInteger counterInit = new AtomicInteger();
        AtomicInteger counterFinal = new AtomicInteger();

        System.out.println("START");

        Ivy bus = new Ivy("agent", "coucou", null);

        bus.start("127.255.255.255:2010");

        Stroke currentStroke = new Stroke();

        bus.bindMsg("Palette:MousePressed x=(.*) y=(.*)", (client, s) -> {
            currentStroke.init();
            Drawer.deleteDraw(counterInit, counterFinal, bus);
            Drawer.startDraw(counterInit, bus, s[0], s[1]);
            currentStroke.addPoint(Integer.parseInt(s[0]), Integer.parseInt(s[1]));

        });
        bus.bindMsg("Palette:MouseDragged x=(.*) y=(.*)", (client, s) -> {
            Drawer.continuDraw(bus, s[0], s[1]);
            currentStroke.addPoint(Integer.parseInt(s[0]), Integer.parseInt(s[1]));
        });

        bus.bindMsg("Palette:MouseReleased x=(.*) y=(.*)", (client, s) -> {
            Drawer.stopDraw(counterFinal, bus, s[0], s[1]);

            currentStroke.normalize();

            String[] options = {"Creer_Rectangle", "Creer_Ellipse", "Deplacer", "Supprimer"};

//            String name = JOptionPane.showInputDialog(null, "Quel nom ?");
            String name = (String) JOptionPane.showInputDialog(null, "Quel nom ?", "Choix du nom",  INFORMATION_MESSAGE, null, options, options[0]);
            if (name != null) {
                strokes.addStroke(currentStroke, name);
                strokes.save();
            }
        });
    }
}
