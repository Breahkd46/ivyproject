# Ivy Project

## Prerequis: 

* Java 1.8
* plantUML

## Fusion multimodale

Apres compilation du projet java :

1. Apprentissage
    * lancer le set up
    * lancer Apprentissage
2. Fusion
    * Lancer le set up
    * lancer Reco
    * lancer Fusion

## Set up

Lancer le set up pour windows avec make
* `make -j set-up-win`

> Sinon lancer :
> ```shell
> java -classpath ./dependencies/ivy.jar;./tools/Palette/palette.jar fr.irit.elipse.enseignement.isia.PaletteGraphique
> java -classpath ./tools/Visionneur/ivy-java-1.2.8rc1.jar;./tools/Visionneur/visionneur1.1.jar fr.irit.diamant.ivy.viewer.Visionneur
> cd sra5 && sra5 -b 127.255.255.255:2010 -p off
> ```

### Liste des etats

* Att_Action
* Att_Option_Creation
* Att_Pointage_Creation
* Att_Designation_Couleur
* Att_Pointage_Sup
* Att_Designation_Voix_Sup
* Att_Option_Sup
* Att_Designation_Position_Deplacer
* Att_Voix_Designation
* Att_Position_Deplacer
* Att_Pointage_Position_Post_Des
* Att_Pointage_Position
* Att_Option_Couleur_Deplacer

### Liste des event

* E_Creer_Rectangle
* E_Creer_Ellipse
* E_Supprimer
* E_Deplacer
* E_Pointage
* E_Voix_Position = "ici", "là", "à cette position"
* E_Voix_Couleur = "rouge", "vert", "noir"...
* E_Voix_Designation_Couleur = "de cette couleur"
* E_Voix_Designation = "cet objet", "ce rectangle" ou "cette ellipse"
* Timer
