
start-palette:
	java -classpath ./dependencies/ivy.jar:./tools/Palette/palette.jar fr.irit.elipse.enseignement.isia.PaletteGraphique

start-palette-win:
	java -classpath ./dependencies/ivy.jar;./tools/Palette/palette.jar fr.irit.elipse.enseignement.isia.PaletteGraphique

start-visionneur:
	java -classpath ./tools/Visionneur/ivy-java-1.2.8rc1.jar:./tools/Visionneur/visionneur1.1.jar fr.irit.diamant.ivy.viewer.Visionneur

start-visionneur-win:
	java -classpath ./tools/Visionneur/ivy-java-1.2.8rc1.jar;./tools/Visionneur/visionneur1.1.jar fr.irit.diamant.ivy.viewer.Visionneur

start-sra5:
	cd sra5 && \
	sra5 -b 127.255.255.255:2010 -p off

set-up-win: start-palette-win start-visionneur-win start-sra5
